let icons = document.querySelectorAll('.fa-eye');
let inputOne = document.getElementById('input-one');
let inputTwo = document.getElementById('input-two');

icons.forEach((el) => el.addEventListener("click", () => { showPassword(el) }))

function showPassword(el) {
    el.classList.toggle('fa-eye-slash')
    let input = el.previousElementSibling;
    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
    } else {
        input.setAttribute('type', 'password');
    }
}

let button = document.querySelector('.btn').addEventListener('click', (e) => {
    document.querySelector('.red-text').hidden = true;
    e.preventDefault();
    if (inputOne.value === inputTwo.value) {      
        alert('You are welcome!');
    } else {
        document.querySelector('.red-text').hidden = false
    }
})
